<x-layout>
    
    <h1>Risultati per : {{$q}} </h1>
    
    <div class="container">
        
        <div class="row justify-content-center">
            
            <div class="col-6 d-flex justify-content-center">
                @foreach ($announcements as $announcement)
                
                <div class="card" style="width: 18rem;">
                    <img src="https://picsum.photos/300" class="card-img-top" alt="...">
                    <div class="card-body">
                        <h5 class="card-title">{{$announcement->title}}</h5>
                        <p class="card-text">{{$announcement->body}}</p>
                        <a href="{{route('home')}}" class="btn btn-primary">Torna alla home</a>
                    </div>
                </div>
                
                @endforeach
            </div>
        </div>
        
    </div>
    
</x-layout>