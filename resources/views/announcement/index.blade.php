<x-layout>

<section class="container">
    <div class="row">
        <div class="col-12">
            <h1>Tutti gli annunci</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-12">

            @foreach ($announcements as $announcement)
                           
            <div class="card">
                <img src="https://picsum.photos/300" class="card-img-top img-fluid" alt="...">
                <div class="card-body">
                  <h5 class="card-title">{{$announcement->title}}</h5>
                  <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                </div>
                <ul class="list-group list-group-flush">
                  <li class="list-group-item">An item</li>
                  <li class="list-group-item">A second item</li>
                  <li class="list-group-item">A third item</li>
                </ul>
                <div class="card-body">
                  <a href="#" class="card-link">Card link</a>
                  <a href="#" class="card-link">Another link</a>
                </div>
            </div>

              @endforeach
        </div>
    </div>    
    
</section>








</x-layout>