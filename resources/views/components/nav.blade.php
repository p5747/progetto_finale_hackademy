
<!-- Navbar social -->
<nav class="mt-3 px-2 navbar navbar-expand-md navbar-search" aria-label="Fourth navbar example">
  <div class="container-fluid">
    <a class="navbar-brand social text-sec ">Seguici sui social</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarsExample04" aria-controls="navbarsExample04" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button> 

    <div class="collapse navbar-collapse" id="navbarsExample04">
      <ul class="navbar-nav ms-auto mb-2 mb-md-0 text-sec">
        <li class="nav-item">
          <i class="fa-2x mx-1 fab fa-facebook"></i>
        </li>
        <li class="nav-item">
          <i class="fa-2x mx-1 fab fa-instagram"></i>
        </li>
        <li class="nav-item">
          <i class="fa-2x mx-1 fab fa-twitter"></i>
        </li>
        <li class="nav-item dropdown">
          <i class="fa-2x mx-1 fab fa-linkedin"></i>
        </li>
      </ul>
      
    </div>
  </div>
</nav>

<!-- Navbar main -->

<nav class="px-2 navbar navbar-main navbar-expand-md navbar-white bg-white" aria-label="Fourth navbar example">
    <div class="container-fluid">
      <a class="navbar-brand" href="{{route('home')}}"><h1 class="presto text-sec">Presto</h1></a>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarsExample04" aria-controls="navbarsExample04" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarsExample04">
        <a class="btn btn-custom text-main ms-auto" href="{{route('announcement.create')}}">+ Annuncio</a>
        <ul class="navbar-nav ms-auto mb-2 mb-md-0">
          <li class="nav-item">
               
          </li>
          <li class="nav-item text-main">
            <a class="nav-link no-decoration text-sec contatti" href="#">Contatti</a>
          </li>
          

          @auth
        
    
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle text-sec" href="#" id="dropdown04" data-bs-toggle="dropdown" aria-expanded="false">{{Auth::user()->name}}</a>
            <ul class="dropdown-menu" aria-labelledby="dropdown04">
              <li><a class="dropdown-item" href="#">Profilo</a></li>
              <li><a class="dropdown-item" href="{{route('logout')}}" onclick="event.preventDefault();
                getElementById('form-logout').submit();">Logout</a></li>
              <form method="POST" id="form-logout" action="{{route('logout')}}" style="display: none;" >
                @csrf
              </form>
              
            </ul>
          </li>
          
          @else

          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle text-sec accedi" href="#" id="dropdown04" data-bs-toggle="dropdown" aria-expanded="false">Accedi</a>
            <ul class="dropdown-menu" aria-labelledby="dropdown04">
              <li><a class="dropdown-item" href="{{route('login')}}">Login</a></li>
              <li><a class="dropdown-item" href="{{route('register')}}">Registrati</a></li>
              
            </ul>
          </li>

          
          
          
          
          
          @endauth
          
        </ul>
        
      </div>
    </div>
  </nav>


<!-- Navbar ricerca -->

  <nav class="p-2 navbar navbar-search navbar-expand-md" aria-label="Fourth navbar example">
    <div class="container-fluid">
      <form method="GET" action="{{route('search')}}" class="d-flex">
        <button type="submit" class='btn btn-custom-search'><i class="fas fa-search text-white"></i></button> 
        <input class="form-control search-bar" name="q" type="text" placeholder="Search" aria-label="Search">
  </form>


  {{-- <label for="cars">Choose a car:</label>
            @foreach ($categories as $category)

                <select name="cars" id="cars">
                  <option value="{{route('announcementsFilterByCategory', compact('category'))}}">Volvo</option>

                </select>
            @endforeach --}}

  <div class="dropdown">
  <button class="btn drop-categorie dropdown-toggle me-4 text-white" type="button" id="dropdownCategoryFilter" data-bs-toggle="dropdown" aria-expanded="false">
    Cerca per categoria
  </button>
  <ul class="dropdown-menu" aria-labelledby="dropdownCategoryFilter">
    <li><a class="dropdown-item" href="#">Abbigliamento</a></li>
    <li><a class="dropdown-item" href="#">Collezionismo</a></li>
    <li><a class="dropdown-item" href="#">Elettronica</a></li>
    <li><a class="dropdown-item" href="#">Fai da te</a></li>
    <li><a class="dropdown-item" href="#">Immobili</a></li>
    <li><a class="dropdown-item" href="#">Lavoro</a></li>
    <li><a class="dropdown-item" href="#">Libri</a></li>
    <li><a class="dropdown-item" href="#">Motori</a></li>
    <li><a class="dropdown-item" href="#">Musica</a></li>
    <li><a class="dropdown-item" href="#">Sport</a></li>
   
  </ul>
</div>





      <div class="collapse navbar-collapse" id="navbarsExample04">
        
        
      </div>
    </div>
  </nav>



  {{-- <form>
    <input class="form-control" type="text" placeholder="Search" aria-label="Search">
  </form> --}}