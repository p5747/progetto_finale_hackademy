<x-layout>
    <body class="text-center">
    
        <main class="form-signin">
          @if ($errors->any())
    <div class="alert alert-danger">
      
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
          @endif
              
          
          <form method="POST" action="{{route('register')}}" enctype="multipart/form-data">
            @csrf
            <h1 class="h3 mb-3 fw-normal">Registrati!</h1>
        
            <div class="form-floating">
              <input name="name" type="text" class="form-control" id="fname" placeholder="Nome" >
              <label for="fname">Nome</label>
            </div>
            <div class="form-floating">
                <input name="surname" type="text" class="form-control" id="fsurname" placeholder="Cognome">
                <label for="fsurname">Cognome</label>
              </div>
            <div class="form-floating">
                <input name="email" type="email" class="form-control" id="femail" placeholder="Email">
                <label for="femail">Indirizzo email</label>
            </div>
            <div class="form-floating">
                <input name="phone" type="tel" class="form-control" id="fphone" placeholder="Telefono">
                <label for="fphone">Telefono</label>
            </div>
            <div class="form-floating">
                <input name="address" type="text" class="form-control" id="faddress" placeholder="Indirizzo">
                <label for="faddress">Indirizzo</label>
            </div>
            <div class="form-floating">
                <input name="img" type="file" class="form-control" id="fimg" placeholder="Immagine profilo">
                <label for="fimg">Immagine del profilo</label>
            </div>
            <div class="form-floating">
              <input name="password" type="password" class="form-control" id="fpassword" placeholder="Password">
              <label for="fpassword">Password</label>
            </div>
            <div class="form-floating">
                <input name="password_confirmation" type="password" class="form-control" id="fpasswordConf" placeholder="Conferma Password">
                <label for="fpasswordConf">Conferma Password</label>
            </div>
        
          
            <button class="w-100 btn btn-lg btn-custom btn-primary " type="submit">Registrati</button>
            <p class="mt-5 mb-3 text-muted">&copy; Bug Fiction H-34 2021</p>
          </form>
        </main>
        
        
            
          </body>
</x-layout>