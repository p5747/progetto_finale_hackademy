
<x-layout>
    
    
    
    
    
    
    
    
    <section class="container-fluid mt-5">
        <div class="row">
            <h1 class="text-color">CATEGORIA: </h1>
            <div class="col-12 d-flex bg-main py-5">



                @foreach ($announcements as $announcement)

                
                    <div class="card mx-2 card-custom text-sec mt-5">
                        <div class="card-body">
                            <h3 class="card-title text-center text-color">{{$announcement->title}}</h3>
                            <h5 class="card-title fs-1 text-sec text-center">{{$announcement->price}} €</h5>
                        </div>
                        <ul class="list-group list-group-flush border-top border">
                            
                            <li class="list-group-item border"><img src="https://via.placeholder.com/300" class="card-img-top img-fluid"   alt="..."></li>
                            <li class="list-group-item text-center fs-5 text-color">CATEGORIA: {{$announcement->category->name}}</li>
                            <li class="list-group-item border text-center fs-6 text-color"><h5>Annuncio:</h5><p class="card-text">{{$announcement->body}}</p></li>
                            <li class="list-group-item text-center fs-5 text-color"><h5>Inserito da:</h5> <strong>{{$announcement->user->name}} </strong> , {{$announcement->created_at->format('d/m/Y')}} </li>
                        
                                
                                
                            </li>
                        </ul>

                        <div class="card-body d-flex justify-content-center">
                            <a href="#" class="card-link btn btn-custom btn-lg btn-primary">Vai</a>
                            
                        </div>
                    </div>
                
                @endforeach 

    

            </div>

            <div class="card-body d-flex justify-content-center">
                <a href="#" class="card-link btn btn-custom btn-lg btn-primary">Vai</a>
            </div>
        </div>
        
    </section>
    
    
    
    
    
    
    
    
    
</x-layout>