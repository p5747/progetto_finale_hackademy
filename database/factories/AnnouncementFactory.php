<?php

namespace Database\Factories;

use App\Models\User;
use App\Models\Category;
use Illuminate\Database\Eloquent\Factories\Factory;

class AnnouncementFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title' => $this->faker->sentence(3),
            'body' => $this->faker->text(100),

            'price'=>$this->faker->randomDigit(),
            'category_id'=> Category::factory(1)->create()->first(),
            'user_id' => User::factory(1)->create()->first(),

        ];
    }
}
