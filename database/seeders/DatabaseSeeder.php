<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        // \App\Models\User::factory(10)->create();
          $categories=[
            0=> ['Abbigliamento'],
            1=> ['Collezionismo'],
            2=> ['Elettronica'],
            3=>['Giardinaggio e fai da te'],
            4=> ['Immobili'],
            5=> ['Lavoro'],
            6=>['Libri'],
            7=>['Motori'],
            8=>['Musica e strumenti musciali'],
            9=>['Sport'],
          ];

            foreach($categories as $category)
            {
                DB::table('categories')->insert([
                    'name'=>$category[0]
                ]);
            }

    }

  
}
