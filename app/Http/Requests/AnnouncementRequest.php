<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AnnouncementRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'=>'required',
            'price'=>'required',
            'body'=>'required',
            'category_id'=>'required',

        ];
    }

    public function messages()
    {
        
        return [
                
                'title.required' => 'Campo obbligatorio',
                'price.required' => 'Campo obbligatorio',
                'body.required' => 'Campo obbligatorio',
                'category_id.required' => 'Campo obbligatorio',
                
                
            ];
    }

}
